/*eslint-env node*/

module.exports = {
  semi: false,
  tabWidth: 2,
  trailingComma: "all",
};
