export interface ServerMessage {
  game_state?: GameState
}

export interface GameState {
  you: Player
  other_players: Player[]
  teams: Team[]
  cards: Card[]
  game_over: boolean
}

export interface Team {
  cards_remaining: number
}

export interface Card {
  text: string
  identity: CardIdentity
  revealed: boolean
}

export type CardIdentity =
  | undefined
  | "Civilian"
  | "Assassin"
  | { Agent: number }
export type Role = "Spy" | "SpyMaster"

export interface Assignment {
  team: number
  role: Role
}

export interface Player {
  name: string
  assignment?: Assignment
}

export interface ClientState {
  game_state: GameState
}
