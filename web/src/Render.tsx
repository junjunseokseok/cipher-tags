import React from "react"
import { CardIdentity } from "./Protocol"

export function displayPlayers(p: string[]) {
  return p.map((p, i) => <div key={i}>{p}</div>)
}

export function displayCardIdentity(i: CardIdentity) {
  switch (typeof i) {
    case "undefined":
      return ""
    case "string":
      return i
    case "object":
      return `${displayTeam(i.Agent)} Agent`
  }
}

export function backgroundColorCardIdentity(i: CardIdentity) {
  switch (i) {
    case null:
    case undefined:
      return "white"
    case "Civilian":
      return "#FCFFDE"
    case "Assassin":
      return "#848484"
    default:
      switch (i.Agent) {
        case 0:
          return "#F1A196"
        case 1:
          return "#92CFF1"
        default:
          return "red"
      }
  }
}

export function displayTeam(team_id: number) {
  switch (team_id) {
    case 0:
      return "Red"
    case 1:
      return "Blue"
    default:
      return team_id.toString()
  }
}
