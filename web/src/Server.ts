import { GameState, Player, ServerMessage } from "./Protocol"

export interface Server {
  joinRoom(roomCode: string): void
  setPlayer(roomCode: string, player: Player): void
  revealCard(roomCode: string, card_index: number): void
  resetGame(roomCode: string): void
  subscribe(callback: (gameState: GameState) => void): void
}

export class ServerStub implements Server {
  private readonly websocket: WebSocket
  constructor(websocket: WebSocket) {
    this.websocket = websocket
  }

  joinRoom(roomCode: string) {
    this.websocket.send(
      JSON.stringify({
        join_game: {
          room_code: roomCode,
        },
      }),
    )
  }

  setPlayer(roomCode: string, player: Player) {
    this.websocket.send(
      JSON.stringify({
        set_player: {
          room_code: roomCode,
          player: {
            name: player.name,
            assignment: player.assignment,
          },
        },
      }),
    )
  }

  revealCard(roomCode: string, card_index: number) {
    this.websocket.send(
      JSON.stringify({
        reveal_card: {
          room_code: roomCode,
          card_index: card_index,
        },
      }),
    )
  }

  resetGame(roomCode: string) {
    this.websocket.send(
      JSON.stringify({
        reset_game: {
          room_code: roomCode,
        },
      }),
    )
  }

  subscribe(callback: (state: GameState) => void) {
    this.websocket.onmessage = ev => {
      callback((JSON.parse(ev.data) as ServerMessage).game_state!)
    }
  }
}
