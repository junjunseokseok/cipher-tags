import React from "react"
import { ClientState } from "../Protocol"
import { backgroundColorCardIdentity } from "../Render"

interface Props {
  clientState: ClientState
  onRevealCardRequested: (card_index: number) => void
  onResetGameRequested: () => void
}

export const Game: React.FC<Props> = ({
  clientState,
  onRevealCardRequested,
  onResetGameRequested,
}) => {
  return (
    <div>
      <div>
        Red agents left: {clientState.game_state.teams[0].cards_remaining}
      </div>
      <div>
        Blue agents left: {clientState.game_state.teams[1].cards_remaining}
      </div>
      Cards:
      {clientState.game_state.game_over ? <div>GAME OVER!</div> : ""}
      <div
        style={{
          display: "grid",
          rowGap: 10,
          columnGap: 10,
          gridTemplateColumns: "100px 100px 100px 100px 100px",
        }}
      >
        {clientState.game_state.cards.map((card, i) => (
          <div
            key={i}
            style={{
              width: "100%",
              height: "50px",
              border: "1px solid black",
              background: backgroundColorCardIdentity(card.identity),
              textAlign: "center",
              textDecoration:
                clientState.game_state.you.assignment?.role === "SpyMaster" &&
                card.revealed
                  ? "line-through"
                  : undefined,
            }}
            onClick={
              card.revealed
                ? undefined
                : () => {
                    onRevealCardRequested(i)
                  }
            }
          >
            {card.text}
          </div>
        ))}
      </div>
      <button onClick={() => onResetGameRequested()}>Reset Game</button>
    </div>
  )
}
