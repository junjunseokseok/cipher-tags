import React, { useState } from "react"
import { ClientState } from "../Protocol"

interface Props {
  clientState: ClientState
  onNameEntry: (nickname: string) => void
}

export const NameEntry: React.FC<Props> = ({ clientState, onNameEntry }) => {
  const [nameInput, setNameInput] = useState(clientState.game_state.you.name)
  return (
    <div>
      <div>
        Name:{" "}
        <input
          value={nameInput}
          onChange={event => setNameInput(event.target.value)}
          onKeyUp={event => {
            if (event.keyCode == 13) {
              onNameEntry(nameInput)
            }
          }}
        />
        <button onClick={() => onNameEntry(nameInput)}>Set Name</button>
      </div>
    </div>
  )
}
