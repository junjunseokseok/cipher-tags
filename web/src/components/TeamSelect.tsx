import React from "react"
import { Assignment, ClientState, Player } from "../Protocol"
import { displayPlayers } from "../Render"

interface Props {
  clientState: ClientState
  onAssignmentRequested: (assignment: Assignment) => void
}

export const TeamSelect: React.FC<Props> = ({
  clientState,
  onAssignmentRequested,
}) => {
  const allPlayers: [Player, string][] = [
    [clientState.game_state.you, `✪${clientState.game_state.you.name}`],
    ...clientState.game_state.other_players.map((p): [Player, string] => [
      p,
      p.name ? p.name : "",
    ]),
  ]
  const unassigned: string[] = []
  const redTeam: string[] = []
  const blueTeam: string[] = []
  const redSpyMasters: string[] = []
  const blueSpyMasters: string[] = []
  allPlayers.forEach(([p, pDisplayName]) => {
    if (p.assignment) {
      switch (p.assignment.team) {
        case 0:
          if (p.assignment.role == "Spy") {
            redTeam.push(pDisplayName)
          } else {
            redSpyMasters.push(pDisplayName)
          }
          break
        case 1:
          if (p.assignment.role == "Spy") {
            blueTeam.push(pDisplayName)
          } else {
            blueSpyMasters.push(pDisplayName)
          }
          break
      }
    } else {
      unassigned.push(pDisplayName)
    }
  })
  return (
    <div>
      <div>Unassigned players:</div>
      <div>{displayPlayers(unassigned)}</div>
      <table>
        <tr>
          <td>
            <div>Red spymaster: {displayPlayers(redSpyMasters)}</div>
            <div>
              <button
                onClick={() =>
                  onAssignmentRequested({
                    team: 0,
                    role: "SpyMaster",
                  })
                }
              >
                Become red spymaster
              </button>
            </div>
          </td>
          <td>
            <div>Blue spymaster: {displayPlayers(blueSpyMasters)}</div>
            <div>
              <button
                onClick={() =>
                  onAssignmentRequested({
                    team: 1,
                    role: "SpyMaster",
                  })
                }
              >
                Become blue spymaster
              </button>
            </div>
          </td>
        </tr>
        <tr>
          <td>
            <div>Red team: {displayPlayers(redTeam)}</div>
            <div>
              <button
                onClick={() =>
                  onAssignmentRequested({
                    team: 0,
                    role: "Spy",
                  })
                }
              >
                Join red team
              </button>
            </div>
          </td>
          <td>
            <div>Blue team: {displayPlayers(blueTeam)}</div>
            <div>
              <button
                onClick={() =>
                  onAssignmentRequested({
                    team: 1,
                    role: "Spy",
                  })
                }
              >
                Join blue team
              </button>
            </div>
          </td>
        </tr>
      </table>
    </div>
  )
}
