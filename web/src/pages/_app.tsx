import NextApp from "next/app"
import React from "react"

export default class App extends NextApp {
  render(): JSX.Element {
    const Page = this.props.Component
    const pageProps = this.props.pageProps
    return (
      <div>
        <h1>Cipher Tags</h1>
        <Page {...pageProps} />
      </div>
    )
  }
}
