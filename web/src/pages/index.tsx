import { NextPage } from "next"
import React, { useState } from "react"

const Index: NextPage = () => {
  const [roomCode, setRoomCode] = useState("")
  return (
    <div>
      <div>
        Enter a new, unique room code to create a room or an existing code to
        join a room.
      </div>
      <div>
        Room code:{" "}
        <input
          value={roomCode}
          onChange={event => {
            setRoomCode(event.target.value)
          }}
        />
      </div>
      <div>
        <a href={`/room/${roomCode}`}>Join</a>
      </div>
    </div>
  )
}

export default Index
