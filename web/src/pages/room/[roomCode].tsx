import { NextPage } from "next"
import { useRouter } from "next/router"
import React, { useEffect, useState } from "react"
import { Assignment, GameState, ClientState } from "../../Protocol"
import { Server, ServerStub } from "../../Server"
import { Game } from "../../components/Game"
import { NameEntry } from "../../components/NameEntry"
import { TeamSelect } from "../../components/TeamSelect"

export function generateClientState(gameState: GameState): ClientState {
  return {
    game_state: gameState,
  }
}

const RoomPage: NextPage = () => {
  const router = useRouter()
  const { roomCode } = router.query
  if (roomCode && typeof roomCode !== "string") {
    throw Error("holeee fug")
  }

  const [clientState, setClientState] = useState<ClientState>()

  const [serverConn, setServerConn] = useState<Server>()
  useEffect(() => {
    /**/
    console.log("openWebSocket called")
    // const ws = new WebSocket("wss://gamesrus-server.herokuapp.com/ws/")
    const ws = new WebSocket("ws://192.168.1.245:8080/ws/")
    const ss = new ServerStub(ws)
    ws.onopen = () => {
      console.log("onOpen")
      setServerConn(ss)
      ss.subscribe(state => {
        setClientState(generateClientState(state))
      })
    }
    ws.onclose = () => {
      setServerConn(undefined)
    }
    /*/
    const fakeServer = new FakeServer()
    fakeServer.subscribe(gameState => {
      setClientState(generateClientState(gameState))
    })
    setServerConn(fakeServer)
    /**/
  }, [])

  useEffect(() => {
    if (serverConn && roomCode) {
      serverConn.joinRoom(roomCode)
    }
  }, [serverConn, roomCode])

  return (
    <div>
      <div>Room Code: &quot;{roomCode}&quot;</div>
      {!serverConn ? <div>Connecting...</div> : ""}
      {serverConn && clientState && clientState.game_state ? (
        <div>
          <NameEntry
            clientState={clientState}
            onNameEntry={nickname => {
              serverConn.setPlayer(roomCode, {
                name: nickname,
                assignment: clientState.game_state.you.assignment,
              })
            }}
          />
          <TeamSelect
            clientState={clientState}
            onAssignmentRequested={(assignment: Assignment) => {
              serverConn.setPlayer(roomCode, {
                name: clientState.game_state.you.name,
                assignment: assignment,
              })
            }}
          />
          <Game
            clientState={clientState}
            onRevealCardRequested={card_id => {
              serverConn.revealCard(roomCode, card_id)
            }}
            onResetGameRequested={() => {
              serverConn.resetGame(roomCode)
            }}
          />
        </div>
      ) : (
        <div></div>
      )}
    </div>
  )
}

export default RoomPage
